// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityActor/AreaDamageComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Entity/BaseEntity.h"


// Sets default values for this component's properties
UAreaDamageComponent::UAreaDamageComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bTickInEditor = true;
}


// Called when the game starts
void UAreaDamageComponent::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(handle, this, &UAreaDamageComponent::DoDamage, rate>0 ? rate : 0.1f, rate>0, startDelay);
	if (!canSelfHit)
	{
		actorToIgnore.AddUnique(GetOwner()->GetOwner());
	}
}


// Called every frame
void UAreaDamageComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (TickType == ELevelTick::LEVELTICK_ViewportsOnly)
	{
		FVector location = GetOwner()->GetActorLocation();
		DrawDebugSphere(GetWorld(), location, radius, 12, FColor::Blue, false, 0, 99, 1);
	}
}

void UAreaDamageComponent::DoDamage()
{
	FVector location = GetOwner()->GetActorLocation();
	TArray<FHitResult> hitResults;
	auto debugDraw = drawDebug? EDrawDebugTrace::ForDuration : EDrawDebugTrace::None;
	bool bIsHit = UKismetSystemLibrary::SphereTraceMultiForObjects(GetWorld(), location, location, radius, objectsTypes, false, actorToIgnore, debugDraw, hitResults, false, FLinearColor::Red, FLinearColor::Green, drawTime);
	if (bIsHit)
	{
		for (int i = 0; i < hitResults.Num(); i++)
		{
			auto entityhit = Cast<ABaseEntity>(hitResults[i].GetActor());
			if (entityhit)
			{
				UE_LOG(LogTemp, Warning, TEXT("AOE Damage to %s"), *entityhit->GetName());
				entityhit->HandleDamage(Damage);
			}
		}
	}
}

