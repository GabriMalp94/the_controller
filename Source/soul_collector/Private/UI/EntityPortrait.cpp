// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/EntityPortrait.h"

void UEntityPortrait::UpdateHealth(int value)
{
	healthInfo->UpdateResourceText(FText::FromString(FString::FromInt(value)));
}

void UEntityPortrait::SetPriceText(int value)
{
	soulPriceInfo->UpdateResourceText(FText::FromString(FString::FromInt(value)));
}
