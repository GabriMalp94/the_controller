// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/ResourceInfo.h"

void UResourceInfo::UpdateResourceText(FText text)
{
	if (resourceText)
	{
		resourceText->SetText(text);
	}
}

void UResourceInfo::NativePreConstruct()
{
	if (resourceImage)
	{
		resourceImage->SetBrushFromTexture(defaultSprite);
	}
}
