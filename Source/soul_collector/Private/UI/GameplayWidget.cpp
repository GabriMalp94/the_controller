// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/GameplayWidget.h"
#include "GameInstance/SoulCollectorGameInstance.h"


void UGameplayWidget::NativeConstruct()
{
	Super::NativeConstruct();

	auto gameInstance = Cast<USoulCollectorGameInstance>(GetGameInstance());
	if (gameInstance)
	{
		UpdateDisplayerdSoul(gameInstance->GetSoulCount());
		gameInstance->OnSoulCountUpdate.AddDynamic(this, &UGameplayWidget::UpdateDisplayerdSoul);
	}
}

void UGameplayWidget::UpdateDisplayerdSoul(int value)
{
	soulResourceInfo->UpdateResourceText(FText::FromString(FString::FromInt(value)));
}
