// Fill out your copyright notice in the Description page of Project Settings.


#include "Notify/ActingNotifyState.h"
#include "Interface/StatusUser.h"


void UActingNotifyState::NotifyBegin(USkeletalMeshComponent* meshComp, UAnimSequenceBase* animation, float totalDuration)
{
	IStatusUser* statusUser = Cast<IStatusUser>(meshComp->GetOwner());
	if (statusUser)
	{
		statusUser->UpdateState((uint8)EStatus::ES_Acting, true);
	}
}

void UActingNotifyState::NotifyEnd(USkeletalMeshComponent* meshComp, UAnimSequenceBase* animation)
{
	IStatusUser* statusUser = Cast<IStatusUser>(meshComp->GetOwner());
	if (statusUser)
	{
		statusUser->UpdateState((uint8)EStatus::ES_Acting, false);
	}
}
