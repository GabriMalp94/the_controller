// Fill out your copyright notice in the Description page of Project Settings.


#include "Notify/SingleAbilitySpawnNotify.h"
#include "AbilityActor/BaseAbilityActor.h"

void USingleAbilitySpawnNotify::Notify(USkeletalMeshComponent* sk_mesh, UAnimSequenceBase*)
{
	const FVector offset = sk_mesh->GetComponentRotation().RotateVector(localOffset);
	const FVector spawnLocation = sk_mesh->GetSocketLocation(bone) + offset;

	FActorSpawnParameters spawnParam;
	spawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	FTransform transform(GetDesiredRotation(sk_mesh), spawnLocation, FVector::OneVector);

	auto ability = sk_mesh->GetWorld()->SpawnActorDeferred<ABaseAbilityActor>(abilityActor, transform);
	ability->SetOwner(sk_mesh->GetOwner());
	ability->FinishSpawning(transform);
}

FRotator USingleAbilitySpawnNotify::GetDesiredRotation(USkeletalMeshComponent* sk_mesh) const
{
	switch (orientation)
	{
	case EOriantationType::OWNER_ORIENTATION:
		return sk_mesh->GetComponentRotation();
	case EOriantationType::BONE_ORIENTATION:
		return sk_mesh->GetBoneQuaternion(bone).Rotator();
	default:
		return FRotator::ZeroRotator;
	}
}
