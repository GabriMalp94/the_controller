// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/CombatCombonent.h"
#include "Kismet/KismetMathLibrary.h"



UCombatComponent::UCombatComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;
}

void UCombatComponent::TickComponent(float DeltaTime, ELevelTick tickType, FActorComponentTickFunction* tickFunction)
{
	for (int i = 0; i < abilities.Num(); i++)
	{
		if (!abilities[i].IsOutOfCooldown())
		{
			abilities[i].HandleCooldown(DeltaTime);
		}
	}
}


void UCombatComponent::Setup(FCombatData* _combatData, USkeletalMeshComponent* _skMesh)
{
	if (!_combatData)
	{
		UE_LOG(LogTemp, Error, TEXT("no entity data setup"));
		return;
	}
	combatData = _combatData;
	currentHealth = combatData->MaxHealth;
	
	for (int i = 0; i < _combatData->Abilities.Num(); i++)
	{
		abilities.Add(FAbilityUser(_combatData->Abilities[i], _skMesh));
	}
}

void UCombatComponent::HandleDamage(float value)
{
	currentHealth -= value;
	OnHealthUpdate.Broadcast(UKismetMathLibrary::Round(currentHealth));
}

void UCombatComponent::DoAbility(int index)
{
	if (abilities.IsValidIndex(index) && abilities[index].IsOutOfCooldown())
	{
		abilities[index].CastAbility();
		abilities[index].GetAbilityMontage();
	}
}

