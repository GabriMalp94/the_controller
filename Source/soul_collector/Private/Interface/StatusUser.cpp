// Fill out your copyright notice in the Description page of Project Settings.


#include "Interface/StatusUser.h"

// Add default functionality here for any IStatusUser functions that are not pure virtual.
void IStatusUser::UpdateState(uint8 es_value, bool flag)
{
	Status = flag ? Status | es_value : Status & ~es_value;
}