// Fill out your copyright notice in the Description page of Project Settings.


#include "GameInstance/SoulCollectorGameInstance.h"

void USoulCollectorGameInstance::CollectSoul(int amount)
{
	soulCount += amount;
	OnSoulCountUpdate.Broadcast(soulCount);
}

bool USoulCollectorGameInstance::HasEnaughSoul(int amount)
{
	return soulCount >= amount;
}

bool USoulCollectorGameInstance::TryToSpend(int amount)
{
	if (!HasEnaughSoul(amount))
	{
		return false;
	}

	soulCount -= amount;
	OnSoulCountUpdate.Broadcast(soulCount);
	return true;
}
