// Fill out your copyright notice in the Description page of Project Settings.


#include "GameMode/MapGenerator.h"
#include "Library/SoulCollectorFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "NavMesh/NavMeshBoundsVolume.h"
#include "NavigationSystem.h"

MapGenerator::MapGenerator()
{
}

MapGenerator::~MapGenerator()
{
}

void MapGenerator::Init(UMapData* _mapData, UWorld* _world)
{
	verify(_mapData != nullptr);
	mapData = _mapData;
	world = _world;
	GenerateMap();
	UpdateNavMeshVolume();
}

void MapGenerator::GetTeleportLocationFromCoordinate(const FVector2D& to, const FVector2D& from, FVector& out_location, FRotator& out_rotation)
{
	FVector2D direction = from - to;
	const ATeleport* dstTeleport = map[to]->GetTeleportByDirection(direction);
	verify(dstTeleport != nullptr);
	map[to]->OnRoomEnter();
	dstTeleport->GetOutTransform(out_location, out_rotation);
}

void MapGenerator::GenerateMap()
{
	ResolveContext(mapData->StartingRoomContext);
	ResolveContext(mapData->GameplayRoomContext);
	ResolveContext(mapData->BossRoomContext);
	CreateRoomLink();
}

void MapGenerator::ResolveContext(const TArray<FRoomContext>& roomContext)
{
	TArray<int> contextResolutionState;
	contextResolutionState.Init(0, roomContext.Num());

	while (true)
	{
		for (FVector2D dir : USoulCollectorFunctionLibrary::Directions)
		{
			TArray<int> unresolveIndexList = GetUnresolvedContextIndexList(roomContext, contextResolutionState);
			if (unresolveIndexList.Num() <= 0)
			{
				return;
			}
			int randomUnresolvedIndex = unresolveIndexList[FMath::RandRange(0, unresolveIndexList.Num() -1)];
			int randomRoomTypeIndex = FMath::RandRange(0, roomContext[randomUnresolvedIndex].roomPossibilities.Num()-1);

			TArray<FVector2D> furhterRoomsByDirection = GetFurtherestByDirection(dir);

			for (FVector2D roomCoordinate : furhterRoomsByDirection)
			{
				if (CalcolateNeighborCoordinate(roomCoordinate))
				{
					SetRoom(roomContext[randomUnresolvedIndex].roomPossibilities[randomRoomTypeIndex], roomCoordinate);
					contextResolutionState[randomUnresolvedIndex] += 1;
					//todo break if room is spawn;
					break;
				}
			}
		}
	}

}

TArray<int> MapGenerator::GetUnresolvedContextIndexList(const TArray<FRoomContext>& roomContext, const TArray<int>& contextResolutionState)
{
	verify(roomContext.Num() == contextResolutionState.Num());

	TArray<int> unresolvedIndexList;

	for (int i = 0; i < roomContext.Num(); i++)
	{
		if (contextResolutionState[i] < roomContext[i].roomAmount)
		{
			unresolvedIndexList.Add(i);
		}
	}
	return unresolvedIndexList;
}

TArray<FVector2D> MapGenerator::GetFurtherestByDirection(FVector2D direction)
{
	TArray<FVector2D> temp_coordinates;

	if (direction.Y != 0)
	{
		temp_coordinates = VerticalFurtherCoordinates(FMath::RoundToInt(direction.Y));
	}
	if (direction.X != 0)
	{
		temp_coordinates = HorizontalFurtherCoordinates(FMath::RoundToInt(direction.X));
	}

	if (temp_coordinates.Num() <= 0)
	{
		temp_coordinates.Add(FVector2D(0, 0));
	}

	return temp_coordinates;
}

TArray<FVector2D> MapGenerator::VerticalFurtherCoordinates(int direction)
{
	TArray<FVector2D> temp_coordinates;
	int currentY = 0;
	for (TPair<FVector2D, ABaseRoom*>& pair : map)
	{
		if (pair.Key.Y == currentY)
		{
			temp_coordinates.Add(pair.Key);
			continue;
		}

		if (direction > 0 ? pair.Key.Y > currentY : pair.Key.Y < currentY)
		{
			temp_coordinates.Empty();
			temp_coordinates.Add(pair.Key);
			currentY = pair.Key.Y;
		}
	}
	return temp_coordinates;
}

TArray<FVector2D> MapGenerator::HorizontalFurtherCoordinates(int direction)
{
	TArray<FVector2D> temp_coordinates;
	int currentX = 0;
	for (TPair<FVector2D, ABaseRoom*>& pair : map)
	{
		if (pair.Key.X == currentX)
		{
			temp_coordinates.Add(pair.Key);
			continue;
		}

		if (direction > 0 ? pair.Key.X > currentX : pair.Key.X < currentX)
		{
			temp_coordinates.Empty();
			temp_coordinates.Add(pair.Key);
			currentX = pair.Key.X;
		}
	}
	return temp_coordinates;
}


void MapGenerator::SetRoom(TSubclassOf<ABaseRoom> _roomToSpawn, FVector2D _coordinates)
{
	FVector2D spacedCoordinates = _coordinates * mapData->RoomSpacing;
	FVector roomLocation(spacedCoordinates.X, spacedCoordinates.Y, 0);

	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	ABaseRoom* roomInstance = world->SpawnActor<ABaseRoom>(_roomToSpawn, roomLocation, FRotator::ZeroRotator, spawnParams);
	map.Add(_coordinates, roomInstance);
	roomInstance->SetRoomCoordinate(_coordinates);
}

bool MapGenerator::CalcolateNeighborCoordinate(FVector2D& referenceCoordinate)
{
	if (map.Num() <= 0)
	{
		return true;
	}
	int randomDirection = FMath::RandRange(0, USoulCollectorFunctionLibrary::Directions.Num() - 1);

	if (FMath::RandRange(0,100) < 5)
	{
		FVector2D dstCoordination = referenceCoordinate + USoulCollectorFunctionLibrary::Directions[randomDirection];
		if (!map.Contains(dstCoordination))
		{
			referenceCoordinate = dstCoordination;
			return true;
		}
	}
	return false;
}

void MapGenerator::CreateRoomLink()
{
	for (const TPair<FVector2D, ABaseRoom*>& pair : map)
	{
		LinkRoomToNeighbors(pair);
	}
}

void MapGenerator::LinkRoomToNeighbors(const TPair<FVector2D, ABaseRoom*>& roomPair)
{
	for (int i = 0; i < USoulCollectorFunctionLibrary::Directions.Num(); i++)
	{
		const FVector2D dstCoordinateToCheck = roomPair.Key + USoulCollectorFunctionLibrary::Directions[i];
		if (map.Contains(dstCoordinateToCheck))
		{
			roomPair.Value->GenerateTeleport(dstCoordinateToCheck, i);
		}
	}
}

void MapGenerator::UpdateNavMeshVolume()
{
	AActor* navBoundActor = UGameplayStatics::GetActorOfClass(world, ANavMeshBoundsVolume::StaticClass());
	ANavMeshBoundsVolume* navBoundVolume = Cast<ANavMeshBoundsVolume>(navBoundActor);
	verify(navBoundVolume);

	float navBoundSize = (GetHighestCoordinate()+1 )* mapData->RoomSpacing;

	navBoundVolume->SetActorScale3D(FVector(navBoundSize, navBoundSize, 100));

	UNavigationSystemV1* navSys = FNavigationSystem::GetCurrent<UNavigationSystemV1>(world);
	navSys->OnNavigationBoundsUpdated(navBoundVolume);
}

const int MapGenerator::GetHighestCoordinate()
{
	int tempValue = 0;
	for (TPair<FVector2D, ABaseRoom*> pair : map)
	{
		int currentX = FMath::Abs(pair.Key.X);
		int currentY = FMath::Abs(pair.Key.Y);

		if (currentX > tempValue)
		{
			tempValue = currentX;
		}
		if (currentY > tempValue)
		{
			tempValue = currentY;
		}
	}
	return tempValue;
}
