// Fill out your copyright notice in the Description page of Project Settings.


#include "GameMode/GameplayGameMode.h"
#include "Entity/BaseEntity.h"

void AGameplayGameMode::BeginPlay()
{
	verify(mapData != nullptr);
	Super::BeginPlay();
	InitGameplayUI();
	mapGenerator.Init(mapData, GetWorld());
	SetGamePhase(EGamePhase::Explore);
}

void AGameplayGameMode::Teleport(AActor* actor, FVector2D& destination, FVector2D& from)
{
	FVector dstLocation;
	FRotator dstRotation;

	mapGenerator.GetTeleportLocationFromCoordinate(destination, from, dstLocation, dstRotation);

	UE_LOG(LogTemp, Warning, TEXT("Teleport %s to: %s"), *actor->GetName(), *dstLocation.ToString());
	actor->TeleportTo(dstLocation, dstRotation);
}

void AGameplayGameMode::SubscribeMapEntity(ABaseEntity* baseEntity)
{
	hostileEntitiesList.Add(baseEntity);
	SetGamePhase(EGamePhase::Engage);
}

void AGameplayGameMode::UnSubscribeEntity(ABaseEntity* baseEntity)
{
	hostileEntitiesList.Remove(baseEntity);
	if (hostileEntitiesList.Num() <= 0)
	{
		SetGamePhase(EGamePhase::Explore);
	}
}

void AGameplayGameMode::InitGameplayUI()
{
	auto ui = CreateWidget<UGameplayWidget>(GetWorld(), gameplayWidget);
	ui->AddToViewport();
}

const void AGameplayGameMode::SetGamePhase(EGamePhase newGamePhase) 
{
	if (newGamePhase != gamePhase)
	{
		UE_LOG(LogTemp, Warning, TEXT("Phase Update"))
		gamePhase = newGamePhase;
		GamePhaseUpdateEvent.Broadcast(gamePhase);
	}
}

