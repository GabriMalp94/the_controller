// Fill out your copyright notice in the Description page of Project Settings.


#include "Rooms/Teleport.h"
#include "Kismet/GameplayStatics.h"
#include "GameMode/GameplayGameMode.h"


// Sets default values
ATeleport::ATeleport()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>("root");

	triggerBox = CreateDefaultSubobject<UBoxComponent>("overlap trigger");
	triggerBox->SetupAttachment(RootComponent);

	outPosition = CreateDefaultSubobject<USceneComponent>("out position");
	outPosition->SetupAttachment(RootComponent);
}

void ATeleport::BeginPlay()
{
	Super::BeginPlay();
	triggerBox->OnComponentBeginOverlap.AddDynamic(this, &ATeleport::OnOverlapBegin);
	gameMode = Cast<AGameplayGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
}

void ATeleport::Init(FVector2D& _destination, FVector2D* from)
{
	verify(from != nullptr);

	destination = _destination;
	roomCoordinates = from;
}

void ATeleport::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	gameMode->Teleport(OtherActor, destination, *roomCoordinates);
}

void ATeleport::GetOutTransform(FVector& out_location, FRotator& out_rotator) const
{
	out_location = outPosition->GetComponentLocation();
	out_rotator = outPosition->GetComponentRotation();
}

