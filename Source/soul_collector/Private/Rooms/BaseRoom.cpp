// Fill out your copyright notice in the Description page of Project Settings.


#include "Rooms/BaseRoom.h"
#include "Entity/BaseEntity.h"
#include "Library/SoulCollectorFunctionLibrary.h"
#include "GameMode/GameplayGameMode.h"
#include "EnvironmentQuery/EnvQueryManager.h"
#include "Data/EncounterData.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABaseRoom::ABaseRoom()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>("root");

	floorStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("floor");
	floorStaticMeshComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ABaseRoom::BeginPlay()
{
	Super::BeginPlay();

	gameMode = Cast<AGameplayGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	verify(gameMode != nullptr);
	gameMode->GamePhaseUpdateEvent.AddDynamic(this, &ABaseRoom::OnGamePhaseUpdate);
}

void ABaseRoom::GenerateTeleport(FVector2D destination, int directionIndex)
{
	auto sockets = floorStaticMeshComponent->GetAllSocketNames();

	verify(sockets.Num() > directionIndex);

	floorStaticMeshComponent->GetSocketTransform(sockets[directionIndex]);

	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	ATeleport* currentTeleport = GetWorld()->SpawnActor<ATeleport>(teleportPrefab, spawnParams);
	currentTeleport->AttachToComponent(floorStaticMeshComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale, sockets[directionIndex]);
	currentTeleport->Init(destination, &roomCoordinates);
	teleports.Add(USoulCollectorFunctionLibrary::Directions[directionIndex], currentTeleport);
}

const ATeleport* ABaseRoom::GetTeleportByDirection(const FVector2D& direction)
{
	if (teleports.Contains(direction))
	{
		return teleports[direction];
	}
	return nullptr;
}

void ABaseRoom::OnRoomEnter() const
{
	if (encounterData)
	{
		//GEngine->AddOnScreenDebugMessage(1, 5, FColor::Magenta, FString::Printf(TEXT("Encounter Start")));

		auto thisObj = this;
		ABaseRoom& obj = const_cast<ABaseRoom&>(*thisObj);
		FEnvQueryRequest request = FEnvQueryRequest(spawnQuery, &obj);
		
		request.Execute(EEnvQueryRunMode::AllMatching, &obj, &ABaseRoom::HandleQueryResult);
	}
}

void ABaseRoom::HandleQueryResult(TSharedPtr<FEnvQueryResult> result)
{
	TArray<FVector> spawnPoints;
	result->GetAllAsLocations(spawnPoints);

	for (int i = 0; i < encounterData->Encounters.Num(); i++)
	{
		ResolveEncounterContext(encounterData->Encounters[i], spawnPoints);
	}
}

void ABaseRoom::ResolveEncounterContext(const FEncounter& encounterContext, TArray<FVector>& spawnLocationList)
{
	FActorSpawnParameters spawnParams = FActorSpawnParameters();
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	for (int i = 0; i < encounterContext.EncounterCount; i++)
	{
		if (spawnLocationList.Num() <= 0)
		{
			return;
		}

		int locationIndex = FMath::RandRange(0, spawnLocationList.Num()-1);
		int enemyIndex = FMath::RandRange(0, encounterContext.EntityTypes.Num()-1);

		FVector position = spawnLocationList[locationIndex] + FVector(0, 0, 50);

		GetWorld()->SpawnActor<ABaseEntity>(encounterContext.EntityTypes[enemyIndex], position, FRotator::ZeroRotator, spawnParams);
		spawnLocationList.RemoveAt(locationIndex);
	}
}

void ABaseRoom::OnGamePhaseUpdate(EGamePhase newPhase)
{
	for (TPair<FVector2D, ATeleport*> pair : teleports)
	{
		pair.Value->SetActorHiddenInGame(newPhase == EGamePhase::Engage);
	}
}

