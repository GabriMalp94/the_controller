// Fill out your copyright notice in the Description page of Project Settings.


#include "Library/AbilityUser.h"
#include "Data/EntityData.h"
#include "GameFramework/Character.h"

FAbilityUser::FAbilityUser(const FAbility &_ability, const USkeletalMeshComponent* _skMesh)
{
	ability = &_ability;
	skMesh = _skMesh;

	verify(ability && skMesh);
}

FAbilityUser::~FAbilityUser()
{
}

UAnimMontage* FAbilityUser::GetAbilityMontage()
{
	return ability->AnimMontage;
}

void FAbilityUser::CastAbility()
{
	skMesh->GetAnimInstance()->Montage_Play(ability->AnimMontage);
	currentCooldown = ability->Cooldown;
}

