// Fill out your copyright notice in the Description page of Project Settings.


#include "Library/SoulCollectorFunctionLibrary.h"

const TArray<FVector2D> USoulCollectorFunctionLibrary::Directions = { FVector2D(1,0), FVector2D(0,1), FVector2D(-1,0), FVector2D(0,-1) };
