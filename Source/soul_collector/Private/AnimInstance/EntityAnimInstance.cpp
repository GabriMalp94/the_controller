// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimInstance/EntityAnimInstance.h"
#include "Entity/BaseEntity.h"


void UEntityAnimInstance::NativeBeginPlay()
{
	baseEntity = Cast<ABaseEntity>(GetOwningActor());
	verify(baseEntity);
}

void UEntityAnimInstance::NativeUpdateAnimation(float delta)
{
	if (baseEntity)
	{
		baseEntity->UpdateMovementReduction(GetCurveValue("Movement Reduction"));
	}
}

float UEntityAnimInstance::GetVelocityLenght() const
{
	if (!baseEntity) return 0;

	return baseEntity->GetVelocity().Length();
}
