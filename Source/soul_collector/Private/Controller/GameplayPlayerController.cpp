// Fill out your copyright notice in the Description page of Project Settings.


#include "Controller/GameplayPlayerController.h"
#include "Kismet/KismetMathLibrary.h"


void AGameplayPlayerController::BeginPlay()
{
	Super::BeginPlay();
	soulCollectorGameInstance = Cast<USoulCollectorGameInstance>(GetGameInstance());

	if (!soulCollectorGameInstance)
	{
		UE_LOG(LogTemp, Error, TEXT("No Soul Collector Game Instance Found"));
	}
	playerDefaultCharacterInstance = Cast<APlayerDefaultCharacter>(GetCharacter());
}

void AGameplayPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent.Get()->BindAxis("Move Vertical", this, &AGameplayPlayerController::OnMoveVerticalMovementInput);
	InputComponent.Get()->BindAxis("Move Horizontal", this, &AGameplayPlayerController::OnMoveHorizontalMovementInput);

	InputComponent.Get()->BindAction("Posses", IE_Pressed, this, &AGameplayPlayerController::OnPossesInput);
	InputComponent.Get()->BindAction("Basic Attack", IE_Pressed, this, &AGameplayPlayerController::OnBasicAbility);
	InputComponent.Get()->BindAction("Special Attack", IE_Pressed, this, &AGameplayPlayerController::OnSpecialAbility);

}

void AGameplayPlayerController::Tick(float DeltaTime)
{
	if (baseEntity)
	{
		MoveEntity();
		baseEntity->PossessedTick(DeltaTime);
	}
	UpdateHitResult();
}

void AGameplayPlayerController::OnPossessedEntityDie()
{
	EnablePlayerDefaultCharacter();
}

void AGameplayPlayerController::UpdateHitResult()
{
	GetHitResultUnderCursor(ECC_Visibility, false, hitResult);
}

void AGameplayPlayerController::OnMoveVerticalMovementInput(float axis)
{
	varticalMovement = axis;
}

void AGameplayPlayerController::OnMoveHorizontalMovementInput(float axis)
{
	horizontalMovement = axis;
}

void AGameplayPlayerController::OnPossesInput()
{
	if (hitResult.bBlockingHit)
	{
		auto targetPawn = Cast<ABaseEntity>(hitResult.GetActor());
		if (targetPawn && soulCollectorGameInstance->TryToSpend(targetPawn->EntityData->soulCost))
		{
			EnablePlayerDefaultCharacter();
			playerDefaultCharacterInstance->GoPosses(targetPawn);
		}
	}
}

void AGameplayPlayerController::OnBasicAbility()
{
	if (baseEntity)
	{
		if (hitResult.GetActor())
		{
			bool bIsTarget = hitResult.GetActor()->ActorHasTag("Target");
			baseEntity->DoAttack(0, (bIsTarget) ? hitResult.GetActor()->GetActorLocation() : hitResult.Location);
		}
		
	}
}

void AGameplayPlayerController::OnSpecialAbility()
{
	if (baseEntity)
	{
		if (hitResult.GetActor())
		{
			bool bIsTarget = hitResult.GetActor()->ActorHasTag("Target");
			baseEntity->DoAttack(1, (bIsTarget) ? hitResult.GetActor()->GetActorLocation() : hitResult.Location);
		}
	}
}

void AGameplayPlayerController::MoveEntity()
{
	FVector direction(varticalMovement, horizontalMovement, 0);
	direction.Normalize();
	baseEntity->DoMovement(direction);
}

void AGameplayPlayerController::EnablePlayerDefaultCharacter()
{
	if (baseEntity)
	{
		baseEntity->OnPlayerControllEnd();
		playerDefaultCharacterInstance->SetActorLocation(baseEntity->GetActorLocation());
	}
	Possess(playerDefaultCharacterInstance);
}


