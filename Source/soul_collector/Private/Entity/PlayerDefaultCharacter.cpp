// Fill out your copyright notice in the Description page of Project Settings.


#include "Entity/PlayerDefaultCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Character.h"

void APlayerDefaultCharacter::PossessedBy(AController* newController)
{
	Super::PossessedBy(newController);
	SetActorHiddenInGame(false);
	SetActorEnableCollision(true);
	Status = (uint8)EStatus::ES_None;
	GetCharacterMovement()->MaxWalkSpeed = 600.0;
	GetCharacterMovement()->MaxAcceleration = 2048.0;
}

void APlayerDefaultCharacter::UnPossessed()
{
	Super::UnPossessed();
	SetActorHiddenInGame(true);
	SetActorEnableCollision(false);
}

void APlayerDefaultCharacter::GoPosses(APawn* target)
{
	Status = (uint8)EStatus::ES_Acting;
	targetToPosses = target;
	GetCharacterMovement()->MaxWalkSpeed = 10000;
	GetCharacterMovement()->MaxAcceleration = 10000;
}

void APlayerDefaultCharacter::Tick(float DeltaTime)
{
	if (targetToPosses)
	{
		if (isInRangeToTarget())
		{
			GetController()->Possess(targetToPosses);
			targetToPosses = nullptr;
		}
		else
		{
			FVector direction = targetToPosses->GetActorLocation() - GetActorLocation();
			direction.Normalize();
			SetActorRotation(FRotationMatrix::MakeFromX(direction).Rotator());
			AddMovementInput(direction);
		}
	}
	Super::Tick(DeltaTime);
}

bool APlayerDefaultCharacter::isInRangeToTarget()
{
	const float sqDistance = FVector::DistSquared(GetActorLocation(), targetToPosses->GetActorLocation());
	return sqDistance <= pow(100, 2);
}
