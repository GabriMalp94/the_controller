// Fill out your copyright notice in the Description page of Project Settings.


#include "Entity/MapBaseEntity.h"
#include "UI/EntityPortrait.h"
#include "Controller/GameplayPlayerController.h"
#include "GameMode/GameplayGameMode.h"


AMapBaseEntity::AMapBaseEntity()
{
	widgetComponent = CreateDefaultSubobject<UWidgetComponent>("Widget Component");
	widgetComponent->SetupAttachment(GetMesh());
}

void AMapBaseEntity::BeginPlay()
{
	Super::BeginPlay();
	UEntityPortrait* entityPortrait = Cast<UEntityPortrait>(widgetComponent->GetWidget());
	if (entityPortrait)
	{
		combatComponent->OnHealthUpdate.AddDynamic(entityPortrait, &UEntityPortrait::UpdateHealth);
		entityPortrait->UpdateHealth(combatComponent->GetCurrentHealth());
		entityPortrait->SetPriceText(EntityData->soulCost);
	}
}

void AMapBaseEntity::PossessedTick(float DeltaTime)
{
	HandleDamage(DeltaTime * GetMaxHealthConsumeValue());
}

void AMapBaseEntity::OnPlayerControllEnd()
{
	defaultAiController->Possess(this);
}

void AMapBaseEntity::OnExhaust()
{
	if (soulCollectorGameInstance)
	{
		soulCollectorGameInstance->CollectSoul(EntityData->entityRewardData.SoulReward);
	}

	auto playerController = Cast<AGameplayPlayerController>(GetController());
	if (playerController)
	{
		playerController->OnPossessedEntityDie();
	}
	Super::OnExhaust();
	GetGameMode()->UnSubscribeEntity(this);
}

void AMapBaseEntity::OnPlayerPossession(AGameplayPlayerController* playerController)
{
	GetGameMode()->UnSubscribeEntity(this);
}

void AMapBaseEntity::OnAIPossession(AController* ai_controller)
{
	if (defaultAiController == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("default ai controller setup: %s"), *ai_controller->GetName())
		defaultAiController = ai_controller;
	}
	GetGameMode()->SubscribeMapEntity(this);
}
