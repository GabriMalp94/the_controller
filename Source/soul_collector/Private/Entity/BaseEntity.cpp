// Fill out your copyright notice in the Description page of Project Settings.


#include "Entity/BaseEntity.h"
#include "controller/GameplayPlayerController.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameMode/GameplayGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/CharacterMovementComponent.h"


// Sets default values
ABaseEntity::ABaseEntity()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	combatComponent = CreateDefaultSubobject<UCombatComponent>("combatComponent");
}

// Called when the game starts or when spawned
void ABaseEntity::BeginPlay()
{
	Super::BeginPlay();
	combatComponent->Setup(&EntityData->CombatData, GetMesh());

	soulCollectorGameInstance = Cast<USoulCollectorGameInstance>(GetGameInstance());
}

void ABaseEntity::PossessedBy(AController* newController)
{
	Super::PossessedBy(newController);
	UE_LOG(LogTemp, Warning, TEXT("%s controller is now %s"), *this->GetName(), *newController->GetName())
	AGameplayPlayerController* gameplayController = Cast<AGameplayPlayerController>(newController);
	if (gameplayController)
	{
		gameplayController->SetBaseEntity(this);
		OnPlayerPossession(gameplayController);
	}
	else
	{
		OnAIPossession(newController);
	}
}

// Called every frame
void ABaseEntity::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABaseEntity::DoMovement(FVector direction)
{
	if (isUnAbleToAct()) return;
	if (direction != FVector::ZeroVector)
	{
		AddMovementInput(direction);
	}
}

void ABaseEntity::DoAttack(int index, FVector cursorPoint)
{
	if (isUnAbleToAct()) return;

	auto actorLocation = GetActorLocation();

	FVector startPoint(actorLocation.X, actorLocation.Y, 0);
	FVector endPoint(cursorPoint.X, cursorPoint.Y, 0);

	FRotator destRotation = UKismetMathLibrary::FindLookAtRotation(startPoint, endPoint);
	SetActorRotation(destRotation);
	combatComponent->DoAbility(index);
}

void ABaseEntity::HandleDamage(float Damage)
{
	if (combatComponent)
	{
		combatComponent->HandleDamage(Damage);
		if (IsExhausted())
		{
			OnExhaust();
		}
	}
}

void ABaseEntity::PossessedTick(float DeltaTime)
{
}

void ABaseEntity::UpdateMovementReduction(float value)
{
	GetCharacterMovement()->MaxWalkSpeed = EntityData->MovementData.MovementSpeed * (1-value);
}


void ABaseEntity::OnExhaust()
{
	Destroy();
}

AGameplayGameMode* ABaseEntity::GetGameMode()
{
	if (!gameMode)
	{
		gameMode = Cast<AGameplayGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	}
	return gameMode;
}
