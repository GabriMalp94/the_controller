// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Animation/AnimMontage.h"
#include "EntityData.generated.h"

USTRUCT()
struct FMovement
{
	GENERATED_BODY();

public:
	float MovementSpeed = 600;
};

USTRUCT()
struct  FAbility
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Category = "Animation")
	UAnimMontage* AnimMontage;

	UPROPERTY(EditAnywhere, Category = "Info")
	float Cooldown = 1;
};

USTRUCT()
struct  FCombatData
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Category = "Resources")
	int MaxHealth = 100;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	TArray<FAbility> Abilities;
};

USTRUCT()
struct  FEntityRewardData
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Category = "Reward")
	int SoulReward;
};

/**
 * 
 */
UCLASS()
class SOUL_COLLECTOR_API UEntityData : public UDataAsset
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, Category = "Combat")
	FCombatData CombatData;

	UPROPERTY(EditAnywhere, Category = "Movement")
	FMovement MovementData;

	UPROPERTY(EditAnywhere, Category = "Reward")
	FEntityRewardData entityRewardData;

	UPROPERTY(EditAnyWhere, Category = "Price")
	int soulCost;

	UPROPERTY(EditAnyWhere, Category = "Possession")
	bool consumeHealthWhilePossessed;
};

