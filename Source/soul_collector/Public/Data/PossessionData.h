// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "PossessionData.generated.h"

/**
 * 
 */
UCLASS()
class SOUL_COLLECTOR_API UPossessionData : public UDataAsset
{
	GENERATED_BODY()
	
public:
	int possessedBodyDamagePerSecond = 10;
};
