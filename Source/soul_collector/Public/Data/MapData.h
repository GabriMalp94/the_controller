// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "MapData.generated.h"

/**
 * 
 */

class ABaseRoom;

USTRUCT(BlueprintType)
struct FRoomContext
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Category = "General")
	int roomAmount;

	UPROPERTY(EditAnywhere, Category = "Types")
	TArray<TSubclassOf<ABaseRoom>> roomPossibilities;
};

UCLASS()
class SOUL_COLLECTOR_API UMapData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnyWhere, Category = "General")
	int32 RoomSpacing = 5000;

public:

	UPROPERTY(EditAnyWhere, Category = "Room")
	TArray<FRoomContext> BossRoomContext;

	UPROPERTY(EditAnyWhere, Category = "Room")
	TArray<FRoomContext> GameplayRoomContext;

	UPROPERTY(EditAnyWhere, Category = "Room")
	TArray<FRoomContext> StartingRoomContext;


};
