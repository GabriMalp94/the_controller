// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "EncounterData.generated.h"

/**
 * 
 */
class ABaseEntity;

USTRUCT(BlueprintType)
struct FEncounter
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
	int EncounterCount;

	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf<ABaseEntity>> EntityTypes;
};

UCLASS()
class SOUL_COLLECTOR_API UEncounterData : public UDataAsset
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere)
	TArray<FEncounter> Encounters;
};
