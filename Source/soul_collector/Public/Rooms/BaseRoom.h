// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Rooms/Teleport.h"
#include "EnvironmentQuery/EnvQuery.h"
#include "BaseRoom.generated.h"

class UEncounterData;
class AGameplayGameMode;
struct FEncounter;
enum EGamePhase;

UCLASS()
class SOUL_COLLECTOR_API ABaseRoom : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseRoom();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* floorStaticMeshComponent;

public:
	void GenerateTeleport(FVector2D destination, int directionIndex);
	void SetRoomCoordinate(FVector2D& _coordinates) { roomCoordinates = _coordinates; };
	const ATeleport* GetTeleportByDirection(const FVector2D& direction);
	void OnRoomEnter() const;


private:
	UPROPERTY(EditAnywhere)
	TSubclassOf<ATeleport> teleportPrefab;
	TMap<FVector2D, ATeleport*> teleports;
	FVector2D roomCoordinates;

	UPROPERTY(EditAnywhere)
	UEncounterData* encounterData;

	AGameplayGameMode* gameMode;

private:
	UPROPERTY(EditAnywhere)
	UEnvQuery* spawnQuery;

	void HandleQueryResult(TSharedPtr<FEnvQueryResult> result);
	void ResolveEncounterContext(const FEncounter& encounterContext, TArray<FVector>& spawnLocationList);

	UFUNCTION()
	void OnGamePhaseUpdate(EGamePhase newPhase);
};
