// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Teleport.generated.h"


class AGameplayGameMode;
UCLASS()
class SOUL_COLLECTOR_API ATeleport : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATeleport();

private:
	UPROPERTY(EditAnywhere)
	UBoxComponent* triggerBox;

	UPROPERTY(EditAnywhere)
	USceneComponent* outPosition;

	FVector2D destination;
	AGameplayGameMode* gameMode;

	FVector2D* roomCoordinates;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	void Init(FVector2D& _destination, FVector2D* from);
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	void GetOutTransform(FVector& out_location, FRotator& out_rotator) const;
};
