// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Entity/BaseEntity.h"
#include "Entity/PlayerDefaultCharacter.h"
#include "Data/PossessionData.h"
#include "GameInstance/SoulCollectorGameInstance.h"
#include "GameplayPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SOUL_COLLECTOR_API AGameplayPlayerController : public APlayerController
{
	GENERATED_BODY()

private:
	ABaseEntity* baseEntity;
	APlayerDefaultCharacter* playerDefaultCharacterInstance;

public:
	virtual void BeginPlay() override;

	virtual void SetupInputComponent() override;

	virtual void Tick(float DeltaTime) override;

	void SetBaseEntity(ABaseEntity* _baseEntity)
	{
		baseEntity = _baseEntity;
	}
	void OnPossessedEntityDie();

private:
	float varticalMovement;
	float horizontalMovement;
	FHitResult hitResult;

	void UpdateHitResult();
	UFUNCTION()
	void OnMoveVerticalMovementInput(float axis);
	UFUNCTION()
	void OnMoveHorizontalMovementInput(float axis);
	UFUNCTION()
	void OnPossesInput();
	UFUNCTION()
	void OnBasicAbility();
	UFUNCTION()
	void OnSpecialAbility();

private:
	void MoveEntity();
	void EnablePlayerDefaultCharacter();

private:
	UPROPERTY(EditAnyWhere)
	UPossessionData* possessionData;
	USoulCollectorGameInstance* soulCollectorGameInstance;
};
