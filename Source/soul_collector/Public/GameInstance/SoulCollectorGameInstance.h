// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "SoulCollectorGameInstance.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSoulUpdateSignature, int, value);

UCLASS()
class SOUL_COLLECTOR_API USoulCollectorGameInstance : public UGameInstance
{
	GENERATED_BODY()

private: 
	int soulCount = 10;

public:
	UPROPERTY()
	FSoulUpdateSignature OnSoulCountUpdate;

	void CollectSoul(int amount);
	bool HasEnaughSoul(int amount);
	bool TryToSpend(int amount);

	int GetSoulCount() const{ return soulCount; }

	UPROPERTY(EditAnywhere)
	float BodyPercentConsumeMaxHealth = 0.05f;
};
