// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UI/ResourceInfo.h"
#include "EntityPortrait.generated.h"

/**
 * 
 */
UCLASS()
class SOUL_COLLECTOR_API UEntityPortrait : public UUserWidget
{
	GENERATED_BODY()

private:
	UPROPERTY(meta = (BindWidget))
	UResourceInfo* healthInfo;

	UPROPERTY(meta = (BindWidget))
	UResourceInfo* soulPriceInfo;

public:
	UFUNCTION()
	void UpdateHealth(int value);
	UFUNCTION()
	void SetPriceText(int value);
};
