// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Engine/Texture2D.h"
#include "ResourceInfo.generated.h"

/**
 * 
 */
UCLASS()
class SOUL_COLLECTOR_API UResourceInfo : public UUserWidget
{
	GENERATED_BODY()

private:
	UPROPERTY(EditAnywhere)
	UTexture2D* defaultSprite;

protected:
	UPROPERTY(meta = (BindWidget))
	UImage* resourceImage;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* resourceText;

public:
	UFUNCTION()
	void UpdateResourceText(FText text);

	virtual void NativePreConstruct() override;
};
