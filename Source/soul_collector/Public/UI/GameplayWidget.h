// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UI/ResourceInfo.h"
#include "GameplayWidget.generated.h"

/**
 * 
 */
UCLASS()
class SOUL_COLLECTOR_API UGameplayWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	virtual void NativeConstruct() override;
private:
	UPROPERTY(meta = (BindWidget))
	UResourceInfo* soulResourceInfo;

private:
	UFUNCTION()
	void UpdateDisplayerdSoul(int value);
};
