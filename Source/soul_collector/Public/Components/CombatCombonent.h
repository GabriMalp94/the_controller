// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Data/EntityData.h"
#include "Library/AbilityUser.h"
#include "CombatCombonent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FResourceUpdateSignature, int, value);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SOUL_COLLECTOR_API UCombatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCombatComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick tickType, FActorComponentTickFunction* tickFunction) override;

public:
	//getter
	float GetHealthFillValue() {return (float)currentHealth / combatData->MaxHealth; }
	float GetCurrentHealth() const { return currentHealth;}

public:
	//events
	FResourceUpdateSignature OnHealthUpdate;
	
private:
	//data
	float currentHealth;
	FCombatData* combatData;
	TArray<FAbilityUser> abilities;

public:
	//actions
	void Setup(FCombatData* _combatData, USkeletalMeshComponent* _skMesh);
	void HandleDamage(float value);
	void DoAbility(int index);
};
