// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"

struct FAbility;
/**
 * 
 */
class SOUL_COLLECTOR_API FAbilityUser
{

public:
	FAbilityUser(const FAbility &_ability, const USkeletalMeshComponent* _skMesh);
	~FAbilityUser();

private:
	const FAbility* ability;
	const USkeletalMeshComponent* skMesh;
	float currentCooldown = 0;

public:
	const float GetCurrentCooldown() const { return currentCooldown; };
	const bool IsOutOfCooldown() const { return currentCooldown <= 0; };
	void HandleCooldown(float deltaTime) { currentCooldown -= deltaTime; };
	UAnimMontage* GetAbilityMontage();
	void CastAbility();
};
