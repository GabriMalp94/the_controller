// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SoulCollectorFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class SOUL_COLLECTOR_API USoulCollectorFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	static const TArray<FVector2D> Directions;
};
