// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "StatusUser.generated.h"

UENUM()
enum class EStatus : uint8
{
	ES_None = 0,
	ES_Acting = 1,
	ES_Stun = 2
};

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UStatusUser : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SOUL_COLLECTOR_API IStatusUser
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	uint8 GetStatus() const { return Status; }
	void UpdateState(uint8 es_value, bool flag);

protected:
	uint8 Status = (uint8)EStatus::ES_None;
};
