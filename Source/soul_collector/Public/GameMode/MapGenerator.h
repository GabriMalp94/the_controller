// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/MapData.h"
#include "Rooms/BaseRoom.h"

/**
 * 
 */

class SOUL_COLLECTOR_API MapGenerator
{
public:
	MapGenerator();
	~MapGenerator();
private:
	UMapData* mapData;
	UWorld* world;
	TMap<FVector2D, ABaseRoom*> map;

public:
	void Init(UMapData* _mapData, UWorld* _world);
	void GetTeleportLocationFromCoordinate(const FVector2D& to, const FVector2D& from, FVector& out_location, FRotator& out_rotation);

private:
	void GenerateMap();
	void ResolveContext(const TArray<FRoomContext>& roomContext);
	TArray<int> GetUnresolvedContextIndexList(const TArray<FRoomContext>& roomContext, const TArray<int>& contextResolutionState);
	TArray<FVector2D> GetFurtherestByDirection(FVector2D direction);
	TArray<FVector2D> VerticalFurtherCoordinates(int direction);
	TArray<FVector2D> HorizontalFurtherCoordinates(int direction);
	void SetRoom(TSubclassOf<ABaseRoom> _roomToSpawn, FVector2D _coordinates);
	bool CalcolateNeighborCoordinate(FVector2D& referenceCoordinate);
	void CreateRoomLink();
	void LinkRoomToNeighbors(const TPair<FVector2D, ABaseRoom*>& roomPair);
	void UpdateNavMeshVolume();
	const int GetHighestCoordinate();
};
