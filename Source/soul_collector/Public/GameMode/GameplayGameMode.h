// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UI/GameplayWidget.h"
#include "Data/MapData.h"
#include "GameMode/MapGenerator.h"
#include "GameplayGameMode.generated.h"

UENUM()
enum EGamePhase
{
	Explore,
	Engage,
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FGamePhaseSignature, EGamePhase, newGamePhase);

class ABaseEntity;

UCLASS()
class SOUL_COLLECTOR_API AGameplayGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	UPROPERTY()
	FGamePhaseSignature GamePhaseUpdateEvent;

public:
	virtual void BeginPlay() override;
	void Teleport(AActor* actor, FVector2D& destination, FVector2D& from);
	void SubscribeMapEntity(ABaseEntity* baseEntity);
	void UnSubscribeEntity(ABaseEntity* baseEntity);

private:
	UPROPERTY(EditAnyWhere, Category = "UI")
	TSubclassOf<UGameplayWidget> gameplayWidget;

	UPROPERTY(EditAnyWhere, Category = "Map")
	UMapData* mapData;

	MapGenerator mapGenerator;
	TArray<ABaseEntity*> hostileEntitiesList;

	EGamePhase gamePhase;

private:
	void InitGameplayUI();
	const void SetGamePhase(EGamePhase gamePhase);
};


