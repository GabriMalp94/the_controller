// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "SingleAbilitySpawnNotify.generated.h"

/**
 * 
 */
class ABaseAbilityActor;

UENUM()
enum class EOriantationType : uint8
{
	NONE,
	OWNER_ORIENTATION,
	BONE_ORIENTATION,

};

UCLASS()
class SOUL_COLLECTOR_API USingleAbilitySpawnNotify : public UAnimNotify
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, Category = "Info")
	TSubclassOf<ABaseAbilityActor> abilityActor;
	
	UPROPERTY(EditAnywhere, Category = "Info")
	FName bone;

	UPROPERTY(EditAnywhere, Category = "Info")
	EOriantationType orientation;

	UPROPERTY(EditAnywhere, Category = "Info")
	FVector localOffset;

public:
	virtual void Notify(USkeletalMeshComponent* sk_mesh, UAnimSequenceBase*) override;

private:
	FRotator GetDesiredRotation(USkeletalMeshComponent* sk_mesh) const;
};
