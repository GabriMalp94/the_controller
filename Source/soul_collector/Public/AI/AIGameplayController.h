// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "AIGameplayController.generated.h"

/**
 * 
 */
UCLASS()
class SOUL_COLLECTOR_API AAIGameplayController : public AAIController
{
	GENERATED_BODY()
};
