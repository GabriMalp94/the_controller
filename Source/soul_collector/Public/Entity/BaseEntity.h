// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Data/EntityData.h"
#include "Components/CombatCombonent.h"
#include "GameInstance/SoulCollectorGameInstance.h"
#include "Interface/StatusUser.h"
#include "BaseEntity.generated.h"


class AGameplayPlayerController;
class AGameplayGameMode;


UCLASS()
class SOUL_COLLECTOR_API ABaseEntity : public ACharacter, public IStatusUser
{
	GENERATED_BODY()
		//ENGINE VIRTUAL

public:
	ABaseEntity();
protected:
	virtual void BeginPlay() override;
public:	
	virtual void PossessedBy(AController* newController) override;
	virtual void Tick(float DeltaTime) override;
//ACTIONS
public:
	void DoMovement(FVector direction);
	void DoAttack(int index, FVector cursorPoint);
	void HandleDamage(float Damage);
	virtual void PossessedTick(float DeltaTime);
	virtual void OnPlayerControllEnd() {};
	void UpdateMovementReduction(float value);
protected:
	virtual void OnExhaust();
	virtual void OnPlayerPossession(AGameplayPlayerController* playerController){};
	virtual void OnAIPossession(AController* ai_controller) {};
//GETTER
public:
	bool IsExhausted() const {return combatComponent->GetCurrentHealth() <= 0; }
protected:
	bool isUnAbleToAct() const { return Status & (uint8)EStatus::ES_Stun || Status & (uint8)EStatus::ES_Acting;};
//DATA
public:
	UPROPERTY(EditAnywhere, Category = "Data")
	UEntityData* EntityData;

protected:
	float actionWeight;
//REFERENCE

private:
	AGameplayGameMode* gameMode;

protected:
	UCombatComponent* combatComponent;
	USoulCollectorGameInstance* soulCollectorGameInstance;

	AGameplayGameMode* GetGameMode();

};

