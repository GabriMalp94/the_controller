// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Entity/BaseEntity.h"
#include "PlayerDefaultCharacter.generated.h"

/**
 * 
 */
UCLASS()
class SOUL_COLLECTOR_API APlayerDefaultCharacter : public ABaseEntity
{
	GENERATED_BODY()
	
public:
	virtual void PossessedBy(AController* newController) override;
	virtual void UnPossessed() override;

	void GoPosses(APawn* target);
	virtual void Tick(float DeltaTime) override;

private:
	APawn* targetToPosses;
	bool isInRangeToTarget();
};
