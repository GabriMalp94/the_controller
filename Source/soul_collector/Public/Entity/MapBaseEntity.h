// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Entity/BaseEntity.h"
#include "Components/WidgetComponent.h"
#include "MapBaseEntity.generated.h"

/**
 * 
 */
UCLASS()
class SOUL_COLLECTOR_API AMapBaseEntity : public ABaseEntity
{
	GENERATED_BODY()
	
public:
	AMapBaseEntity();
protected:
	virtual void BeginPlay() override;
//ACTIONS
public:
	virtual void PossessedTick(float DeltaTime) override;
	virtual void OnPlayerControllEnd() override;

protected:
	virtual void OnExhaust() override;
	virtual void OnPlayerPossession(AGameplayPlayerController* playerController) override;
	virtual void OnAIPossession(AController* ai_controller) override;
//GETTER
private:
	float GetMaxHealthConsumeValue() const { return EntityData->CombatData.MaxHealth * soulCollectorGameInstance->BodyPercentConsumeMaxHealth; }
//REFERENCE
private:
	UPROPERTY(EditAnywhere)
	UWidgetComponent* widgetComponent;

	AController* defaultAiController;
};
