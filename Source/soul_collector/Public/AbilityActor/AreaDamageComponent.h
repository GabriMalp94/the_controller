// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AreaDamageComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SOUL_COLLECTOR_API UAreaDamageComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAreaDamageComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere, Category = "Info")
	float Damage;

	UPROPERTY(EditAnywhere, Category = "Info")
	float radius;

	UPROPERTY(EditAnywhere, Category = "Info")
	bool canSelfHit = false;

	UPROPERTY(EditAnywhere, Category = "Timer")
	float startDelay;

	UPROPERTY(EditAnywhere, Category = "Timer")
	float rate;

	UPROPERTY(EditAnywhere, Category = "Debug")
	bool drawDebug = false;

	UPROPERTY(EditAnywhere, Category = "Debug")
	float drawTime = 1;

	FTimerHandle handle;

	UPROPERTY(EditAnywhere, Category = "Query");
	TArray<TEnumAsByte<EObjectTypeQuery>> objectsTypes;

	TArray<AActor*> actorToIgnore;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
private:
	UFUNCTION()
	void DoDamage();
};
