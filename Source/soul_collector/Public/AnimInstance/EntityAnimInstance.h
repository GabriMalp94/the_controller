// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "EntityAnimInstance.generated.h"

/**
 * 
 */
class ABaseEntity;

UCLASS()
class SOUL_COLLECTOR_API UEntityAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	virtual void NativeBeginPlay() override;
	virtual void NativeUpdateAnimation(float delta) override;

	UFUNCTION(BlueprintPure, Category = "Utils")
	float GetVelocityLenght() const;

private:
	ABaseEntity* baseEntity;
};
