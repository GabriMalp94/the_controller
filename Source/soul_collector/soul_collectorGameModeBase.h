// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "soul_collectorGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SOUL_COLLECTOR_API Asoul_collectorGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
